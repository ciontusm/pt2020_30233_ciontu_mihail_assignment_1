
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

public class View extends Parent {
    Delegate delegate;

    Label polinom1 = new Label("");
    Label polinom2 = new Label("");
    Label polinom3 = new Label("");

    TextField monom1 = new TextField();

    TextField monom2 = new TextField();

    Label result = new Label();

    private void complexBuilder(VBox container,int op){
        HBox polinomContainer = new HBox();
        HBox polinomViewer=new HBox();
        HBox polinomResultContainer = new HBox();

        Button monom1Btn = new Button("Add Monom");
        monom1Btn.addEventHandler(MouseEvent.MOUSE_CLICKED, (event) -> {
            if (this.delegate != null) {
                this.delegate.handleEventSetPolinomComplex(event);
            }
        });

        Button equalBtn = new Button("=");
        equalBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, (event2) -> {
            if (this.delegate != null) {
                this.delegate.handleEventEqualComplex(event2,op);
            }
        });

        polinomViewer.getChildren().add(polinom1);
        //Polinom Input HBox
        polinomContainer.getChildren().addAll(monom1, monom1Btn);


        container.getChildren().addAll(polinomContainer,
                polinomViewer,equalBtn,result);

        container.setStyle("-fx-background-color: #808080");

        polinom1.setTextFill(Color.web("#FFFFFF"));
        polinom2.setTextFill(Color.web("#FFFFFF"));
        labelStyler(polinom1);
        labelStyler(polinom2);
        labelStyler(result);
    }
    private void simpleBuilder(VBox container,int op){
        //Monom for first polinom
        Button monom1Btn = new Button("Add Monom");
        monom1Btn.addEventHandler(MouseEvent.MOUSE_CLICKED, (event2) -> {
                this.delegate.handleEventSetPolinom1(event2);
        });
        //Monom for secound polinom
        Button monom2Btn = new Button("Add Monom");
        monom2Btn.addEventHandler(MouseEvent.MOUSE_CLICKED, (event2) -> {
                this.delegate.handleEventSetPolinom2(event2);
        });
        Button equalBtn = new Button("=");
        equalBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, (event2) -> {
                this.delegate.handleEventEqualSimple(event2,op);
        });

        HBox firstPolinomContainer = new HBox();
        HBox secoundPolinomContainer = new HBox();
        HBox secoundPolinomResultContainer = new HBox();
        secoundPolinomResultContainer.getChildren().addAll(polinom2, equalBtn, result, polinom3);
        //Polinom Input HBox
        firstPolinomContainer.getChildren().addAll(monom1, monom1Btn);
        secoundPolinomContainer.getChildren().addAll(monom2, monom2Btn);

        container.getChildren().addAll(firstPolinomContainer,
                secoundPolinomContainer, equalBtn,polinom1,polinom2,result);

        container.setStyle("-fx-background-color: #808080");

        polinom1.setTextFill(Color.web("#FFFFFF"));
        polinom2.setTextFill(Color.web("#FFFFFF"));
        labelStyler(polinom1);
        labelStyler(polinom2);
        labelStyler(result);

    }
    public VBox builder() {

        Menu menu1 = new Menu("Operatii simple");
        Menu menu2 = new Menu("Operatii complexe");
        Menu menu3=new Menu("Restart");
        MenuItem menuIntegrationItem=new MenuItem("Integration");

        MenuItem menuDerivationItem=new MenuItem("Derivation");

        MenuItem menuSubItem = new MenuItem("Sub");

        MenuItem menuAddItem = new MenuItem("Add");
        MenuItem restart=new MenuItem("Restart");
        MenuItem menuMultItem=new MenuItem("Multiply");
        menu1.getItems().addAll(menuSubItem,menuAddItem,menuMultItem);
        menu2.getItems().addAll(menuDerivationItem,menuIntegrationItem);
        menu3.getItems().add(new MenuItem("Restart"));
        MenuBar menuBar = new MenuBar();
        menuBar.getMenus().addAll(menu1, menu2, menu3);

        menu3.setOnShown(event->{
                this.delegate.restart(event);
        });
        VBox container = new VBox(menuBar);

        menuIntegrationItem.setOnAction(event -> {
            complexBuilder(container,0);
        });
        menuDerivationItem.setOnAction(event -> {
            complexBuilder(container,1);
        });
        menuSubItem.setOnAction(event -> {
            simpleBuilder(container,1);
        });
        menuAddItem.setOnAction(event->{
            simpleBuilder(container,0);
        });
        menuMultItem.setOnAction((event)->{
            simpleBuilder(container,2);
        });
        return container;
    }

    public void setPolinom(Label polinom,String prefix, int power) {
        String sign;
        if (polinom.getText() != "" &&
                String.valueOf(polinom.getText().charAt(0)).matches("[^-]"))
            sign = "+";
        else
            sign = "";


        if (prefix.equals("1")) {

            prefix = "";
        }
        if (power == 1)
            if (prefix.equals("0"))
                polinom.setText("");
            else
            polinom.setText(prefix + "x");
        else {
            if (prefix.equals("0"))
                polinom.setText(""+sign+polinom.getText());
            else
            polinom.setText(prefix + "x" + "^" + power + sign + polinom.getText());
        }
    }

    public void setPolinomResult(String result) {
        this.result.setText(result);
    }
    public String getMonom1(){
        return monom1.getText();
    }

    public String getMonom2(){
        return monom2.getText();
    }
    public Label getPolinom1(){return polinom1;}
    public Label getPolinom2(){return polinom2;}
    public void labelStyler(Label l){
        l.setStyle(
                "    -fx-font:14pt \"Arial\";\n" +
                        "    -fx-background-insets: 10px;\n" +
                        "    -fx-padding:15");
    }
}
