import Model.Polynomial;
import org.junit.Test;

import java.util.*;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
public class Junit {
    Polynomial polynom1=new Polynomial();

    Polynomial polynom2=new Polynomial();
    public void initialize(){
        //2X^2+X
        polynom1.addMonomToList(1);
        polynom1.addMonomToList(2);
        //2X^2+X
        polynom2.addMonomToList(1);
        polynom2.addMonomToList(1);
    }
    @Test
    public void test_getMonomList(){
        initialize();
        assertThat((int)polynom1.getMonomList().get(0).getPrefix(),equalTo(1));
        assertThat((int)polynom1.getMonomList().get(1).getPrefix(),equalTo(2));
        assertThat(polynom1.getMonomList().get(0).getPower(),equalTo(1));
    }
    @Test
    public void test_Add(){
        initialize();
        Polynomial result;
        result=polynom1.addPolynomTo(polynom2);
        assertThat((int)result.getMonomList().get(0).getPrefix(),equalTo(2));
        assertThat((int)result.getMonomList().get(1).getPrefix(),equalTo(3));
    }

    @Test
    public void test_Sub(){
        initialize();
        Polynomial result;
        result=polynom1.subPolynomFrom(polynom2);
        assertThat((int)result.getMonomList().get(0).getPrefix(),equalTo(0));
        assertThat((int)result.getMonomList().get(1).getPrefix(),equalTo(1));
    }
    @Test
    public void test_Mult(){
        initialize();
        Polynomial result;
        result=polynom1.multiplyPolynomBy(polynom2);

        assertThat((int)result.getMonomList().get(0).getPrefix(),equalTo(1));
        assertThat((int)result.getMonomList().get(1).getPrefix(),equalTo(1));
        assertThat((int)result.getMonomList().get(2).getPrefix(),equalTo(2));
        assertThat((int)result.getMonomList().get(3).getPrefix(),equalTo(2));
    }
    @Test
    public void derivation(){
        initialize();
        Polynomial result;
        result=polynom1.derive();
        assertThat((int)result.getMonomList().get(0).getPrefix(),equalTo(1));
        assertThat((int)result.getMonomList().get(1).getPrefix(),equalTo(4));
    }
    @Test
    public void integration(){
        initialize();
        Polynomial result;
        result=polynom1.integral();
        assertThat(result.getMonomList().get(0).getPrefix(),equalTo(1.0));
        assertThat(result.getMonomList().get(1).getPrefix(),equalTo(1.0));
    }
}
