import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Orientation;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.concurrent.Callable;

public class Main extends Application {
private static Stage stage;
    @Override
    public void start(Stage primaryStage) throws Exception{
        Controller c=new Controller();
        c.buildView(primaryStage);
        setStage(primaryStage);
    }
    public void setStage(Stage stage){
        this.stage=stage;
    }
    public static Stage getStage(){
        return stage;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
