import javafx.event.Event;

public interface Delegate {
    void handleEventSetPolinom1(Event event);
    void handleEventSetPolinom2(Event event);
    void handleEventEqualSimple(Event event,int operation);
    void handleEventSetPolinomComplex(Event event);
    void handleEventEqualComplex(Event event,int operation);
    void restart(Event event);
}
