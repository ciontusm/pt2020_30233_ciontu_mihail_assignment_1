import Model.Polynomial;
import javafx.application.Platform;
import javafx.event.Event;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.util.ArrayList;

public class Controller implements Delegate {
    View root=new View();

    Polynomial p1=new Polynomial();
    Polynomial p2=new Polynomial();

    public void buildView(Stage stage){

        stage.setTitle("Polinom Calculator");
        root.delegate=this;
        Scene scene = new Scene(root.builder(), 960, 600);

        scene.getStylesheets().add("styles.css");
        stage.setScene(scene);
        stage.show();
    }
    public void handlePolinomResultSimple(Polynomial polynomialResult){
        String result = "";
        String potentialResult="";

        for(Polynomial.Monom p : polynomialResult.getMonomList()){
            if (p.getPrefix()>0)
                result=result+"+";
            if(p.getPrefix()==0)
                continue;
            potentialResult=(int)p.getPrefix()+"x"+"^"+p.getPower();
            if (String.valueOf(potentialResult.charAt(0)).matches("[+]")) {
                potentialResult = potentialResult.substring(1);
            }
            if (String.valueOf(potentialResult.charAt(0)).matches("1")) {
                potentialResult = potentialResult.substring(1);
            }
            if (String.valueOf(potentialResult.charAt(0)).matches("[-1]")) {
                potentialResult = "-"+potentialResult.substring(2);
            }
            result=result+potentialResult;
        }
        String finalResult=String.valueOf(result.charAt(0)).matches("[+]") ? result.substring(1) : result;
        root.setPolinomResult(finalResult);
    }
    public void handlePolinomResultComplex(Polynomial polynomialResult, int complexOperation){
        String result = "";
        String potentialResult="";
        for(Polynomial.Monom p : polynomialResult.getMonomList()){
            if (p.getPrefix()>0)
                result=result+"+";
            if(p.getPrefix()==0)
                continue;
            if(complexOperation==1) {
                potentialResult=(int)p.getPrefix()+(p.getPower()==0 ? "" :"x^"+p.getPower());
            }
            else{
                double resultCoeficient=(p.getPrefix()*(p.getPower()-1))/p.getPower();
                if(resultCoeficient==Math.floor(resultCoeficient))
                    potentialResult=((int)resultCoeficient+"x^"+p.getPower());
                else
                potentialResult = (int) (p.getPrefix() * (p.getPower()-1)) + "/" + p.getPower() + "*" + "x" + "^" + p.getPower();
            }
            if (String.valueOf(potentialResult.charAt(0)).matches("[+]")) {
                potentialResult = potentialResult.substring(1);
            }
            result=result+potentialResult;
        }
        String firstChar=String.valueOf(result.charAt(0));
        String finalResult=firstChar.matches("[+]") ? result.substring(1) : result;

        root.setPolinomResult(finalResult);
    }
    @Override
    public void handleEventSetPolinom1(Event event) {
        String textField=root.getMonom1();

        root.setPolinom(root.getPolinom1(),textField,p1.getMonomList().size()+1);

        p1.addMonomToList( Integer.parseInt(textField));
    }

    @Override
    public void handleEventSetPolinom2(Event event) {
        String textField2=root.getMonom2();

        root.setPolinom(root.getPolinom2(),textField2,p2.getMonomList().size()+1);

        p2.addMonomToList(Integer.parseInt(textField2));
    }

    @Override
    public void handleEventEqualSimple(Event event,int operation) {

        Polynomial polynomialResult;

        if(p1.getCurrentPower()>p2.getCurrentPower())
        p2= Polynomial.equalizePolinom(p2,p1);
        else p1= Polynomial.equalizePolinom(p1,p2);

        if(operation!=2)
            polynomialResult=operation==0 ? p1.addPolynomTo(p2) :p1.subPolynomFrom(p2);

        else
            polynomialResult=p1.multiplyPolynomBy(p2);



         handlePolinomResultSimple(polynomialResult);
    }

    @Override
    public void handleEventSetPolinomComplex(Event event) {
        String textField=root.getMonom1();

        root.setPolinom(root.getPolinom1(),textField,p1.getMonomList().size()+1);

        p1.addMonomToList( Integer.parseInt(textField));
    }

    @Override
    public void handleEventEqualComplex(Event event, int operation) {
        Polynomial polynomialResult;
        if (operation == 0)
            polynomialResult=p1.integral();
        else {
            polynomialResult=p1.derive();
        }
        handlePolinomResultComplex(polynomialResult,operation);

    }

    @Override
    public void restart(Event event) {
        System.out.println("pressed");
        Main.getStage().close();
        Platform.runLater(()-> {
            try {
                new Main().start(new Stage());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

    }

}
