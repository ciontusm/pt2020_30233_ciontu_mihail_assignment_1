package Model;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class Polynomial {
    private ArrayList<Monom> MonomList;
    private int currentPower;
    public Polynomial(){
    this.MonomList=new ArrayList<>();
    this.currentPower=0;
    }

    public static Polynomial equalizePolinom(Polynomial p1, Polynomial p2)
    {
        for(int i=p1.MonomList.size();i<p2.MonomList.size();i++){
            p1.addMonomToList(0);
        }
        return p1;
    }
    public void addMonomToList(double prefix){
        this.MonomList.add(new Monom(prefix,++currentPower));
    }
    public void addMonomToList(double prefix,int power){
        this.MonomList.add(new Monom(prefix,power));
    }

    public ArrayList<Monom> getMonomList(){
        return this.MonomList;
    }

    public Polynomial addPolynomTo(Polynomial p){
        ArrayList<Monom> monomList=p.getMonomList();
        ArrayList<Monom> thisList=this.getMonomList();
        Polynomial result=new Polynomial();
        for(int i=0;i<monomList.size();i++){
            Monom m=thisList.get(i).addMonomTo(monomList.get(i));
            result.addMonomToList(m.getPrefix(),m.getPower());
        }
        return result;
    }
    public Polynomial subPolynomFrom(Polynomial p ){
        ArrayList<Monom> monomList=p.getMonomList();
        ArrayList<Monom> thisList=this.getMonomList();
        Polynomial result=new Polynomial();
        for(int i=0;i<monomList.size();i++){
            Monom m=thisList.get(i).subMonomFrom(monomList.get(i));
            result.addMonomToList(m.getPrefix(),m.getPower());
        }
        return result;
    }
    public Polynomial multiplyPolynomBy(Polynomial p){
        ArrayList<Monom> monomList=p.getMonomList();
        Polynomial result=new Polynomial();
        for(int i=0;i<this.MonomList.size();i++){
            for(int j=0;j<monomList.size();j++){
                Monom m=this.MonomList.get(i).multiplyBy(monomList.get(j));
                result.addMonomToList(m.getPrefix(),m.getPower());
            }
        }
        return result;
    }
    public Polynomial derive(){
        Polynomial result=new Polynomial();
        for(int i=0;i<this.MonomList.size();i++){
            Monom m=this.MonomList.get(i).derive();
            result.addMonomToList(m.getPrefix(),m.getPower());
        }
        return result;
    }
    public Polynomial integral(){
        Polynomial result=new Polynomial();
        for(int i=0;i<this.MonomList.size();i++)
        {
            Monom m=this.MonomList.get(i).integral();
            result.addMonomToList(m.getPrefix(),m.getPower());
        }
        return result;
    }
    public int getCurrentPower(){
        return this.currentPower;
    }

    public class Monom {
        private double prefix;
        private int power;
        public Monom(double prefix,int power){
            this.prefix=prefix;
            this.power=power;
        }
        public Monom addMonomTo(Monom m){
            return new Monom(this.prefix+m.getPrefix(),power);
        }
        public Monom subMonomFrom(Monom m){
            return new Monom(this.prefix-m.getPrefix(),power);
        }
        public Monom multiplyBy(Monom m){
            return new Monom(this.prefix*m.getPrefix(),this.power+m.getPower());
        }
        public Monom derive(){
            return new Monom(this.getPrefix()*this.getPower(),this.getPower()-1);
        }
        public Monom integral(){
            return new Monom(this.getPrefix()/(this.getPower()*1.0),this.getPower()+1);
        }
        public double getPrefix(){
            return this.prefix;
        }
        public int getPower(){
            return this.power;
        }

    }
}
